#!/usr/bin/env sh

var "SSH_AUTH_SOCK" "/run/user/1000/gnupg/S.gpg-agent.ssh"
var "SSH_ASKPASS" "/usr/bin/pinentry-gnome3"
