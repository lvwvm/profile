#!/usr/bin/env sh

var "GNUPGHOME" "${XDH}/gnupg"
var "GNUPG_AGENT_CONF" "${XCH}/gnupg/gpg-agent.conf"
var "GNUPG_CONF" "${XCH}/gnupg/gpg.conf"
